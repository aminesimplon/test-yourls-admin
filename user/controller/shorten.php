<?php

// Inclure le fichier de configuration YOURLS
require_once( dirname( __FILE__ ) . '/includes/load-yourls.php' );

// Récupérer le jeton de signature depuis la configuration YOURLS
$signature_token = '1002a612b4';

// Vérifier si des données JSON POST ont été envoyées
$post_data = file_get_contents("php://input");
if (!empty($post_data)) {
    // Convertir les données JSON en tableau associatif
    $data = json_decode($post_data, true);
    if (isset($data['url'])) {
        // Récupérer l'URL à raccourcir depuis les données JSON
        $long_url = $data['url'];
        // Construire la requête API avec le jeton de signature
        $api_url = YOURLS_SITE . '/yourls-api.php?signature=' . $signature_token . '&action=shorturl&url=' . urlencode($long_url);
        // Effectuer la requête API
        $api_response = file_get_contents($api_url);
        // Convertir la réponse en JSON et l'afficher
        header('Content-Type: application/json');
        echo $api_response;
    } else {
        // En cas de données JSON mal formatées, renvoyer un message d'erreur JSON
        $response = array('status' => 'error', 'message' => 'Invalid JSON data');
        echo json_encode($response);
    }
} else {
    // Si aucune donnée JSON n'a été envoyée, renvoyer un message d'erreur JSON
    $response = array('status' => 'error', 'message' => 'No JSON data received');
    echo json_encode($response);
}
